^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package auv_msgs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Forthcoming
-----------
* CMakeList modified to compile action files
* gitignore file added
* new package file. This file replaces manifest (ROS fuerte)
* AUV messages and services modified to be compiled in ROS Groovy
* Removed .svn folders
* auv_msgs with applied patch for cola2
* Contributors: Narcis Palomeras, juandhv
